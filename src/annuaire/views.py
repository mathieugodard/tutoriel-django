from .models import User, Organization, Criterion, Need
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer, OrganizationSerializer, CriterionSerializer, NeedSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]


class OrganizationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows organizations to be viewed or edited.
    """
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    # permission_classes = [permissions.AllowAny]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class CriterionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows criterions to be viewed or edited.
    """
    queryset = Criterion.objects.all()
    serializer_class = CriterionSerializer
    # permission_classes = [permissions.AllowAny]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class NeedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows needs to be viewed or edited.
    """
    queryset = Need.objects.all()
    serializer_class = NeedSerializer
    # permission_classes = [permissions.AllowAny]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


from django.http import HttpResponse

def index(request):
    return HttpResponse("Cette page n'est pas l'objet d'un site")